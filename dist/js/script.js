"use strict";

function testWebP(callback) {
  var webP = new Image();
  webP.onload = webP.onerror = function () {
    callback(webP.height == 2);
  };
  webP.src =
    "data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA";
}

testWebP(function (support) {
  if (support == true) {
    document.querySelector("body").classList.add("webp");
  } else {
    document.querySelector("body").classList.add("no-webp");
  }
});

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

function burgerOpen() {
  const burger = document.querySelector(".header-topmenu__burger");
  let topMenu = document.querySelector(".header-topmenu__list");
  let bl = document.querySelector(".header-topmenu__burger__line");

  burger.addEventListener("click", () => {
    bl.classList.toggle("header-topmenu__burger__line-hide");
    burger.classList.toggle("header-topmenu__burger-active");
    topMenu.classList.toggle("header-topmenu__list-active");
    document.body.classList.toggle("lock");
  });

  document.addEventListener("click", outsideEvtListener);

  function outsideEvtListener(e) {
    if (
      e.target === topMenu ||
      topMenu.contains(e.target) ||
      e.target === burger ||
      e.target === bl
    ) {
      return;
    }
    bl.classList.remove("header-topmenu__burger__line-hide");
    burger.classList.remove("header-topmenu__burger-active");
    topMenu.classList.remove("header-topmenu__list-active");
    document.body.classList.remove("lock");
  }
}
burgerOpen();
